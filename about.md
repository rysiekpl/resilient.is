---
title: "About LibResilient"
layout: page
---

A browser-based decentralized content delivery network, implemented as a JavaScript library to be deployed easily on any website. LibResilient uses [Service Workers](https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API/Using_Service_Workers) and a suite of unconventional in-browser delivery mechanisms, with a strong focus on decentralized tools methods.

**Ideally, site visitors should not need to install any special software nor change any browser or system settings to continue being able to access an overloaded or otherwise inaccessible LibResilient-enabled site, as long as they were able to access it *once* before.**

The project is generously supported by [a grant from the NLnet Foundation](https://nlnet.nl/project/libresilient/).

## Rationale

While a number of content delivery technologies exist, these typically require enormous centralized services. This creates opportunities for gate-keeping, and [causes any disruption at these centralized providers to become a major problem for thousands of websites](https://blog.cloudflare.com/cloudflare-outage-on-july-17-2020/).

This project explores the possibility of solving this issue in a way that would not require website visitors to install any special software or change any settings; the only things that site visitors need are a modern Web browser and the ability to visit a website once, so that the JavaScript Service Worker kicks in.

## Documentation

You can read more in-depth overview of LibResilient [here](../docs/ARCHITECTURE/). And [here](../docs/PHILOSOPHY/) you can find the document describing the philosophy influencing project goals and relevant technical decisions.

And if you're eager to try LibResilient out, the [Quickstart guide](../docs/QUICKSTART/) should help.

<div class="home-nav">
    <a href="../docs/">Docs</a>
</div>

## Current status and project updates

LibResilient is currently considered *beta*: the code works, and the API is mostly stable, but it has not been deployed in production and would benefit from real-world testing. During development it has been tested on Firefox, Chromium and Chrome on desktop, as well as Firefox for mobile on Android, but it should work in any browser implementing the Service Worker API.

You can find news about major project-related developments on the blog.

<div class="home-nav">
    <a href="../blog/">Blog</a>
</div>

## Getting technical

LibResilient is free and open source software, [licensed under the GNU Affero GPL v.3 license](https://gitlab.com/rysiekpl/libresilient/-/blob/master/LICENSE). It is written almost entirely in JavaScript. Code is publicly hosted [on Gitlab](https://gitlab.com/rysiekpl/libresilient/), which is also where [issues are tracked](https://gitlab.com/rysiekpl/libresilient/-/issues/).

<div class="home-nav">
    <a href="https://gitlab.com/rysiekpl/libresilient/">Code</a>
</div>

----

## Site Privacy

This site does not gather personal data other than what is strictly necessary to serve the content (that is, your IP address and time of your visit). No cookies are set, no trackers (neither third-party, nor self-hosted) are used.

Website logs are imported into a local, self-hosted [Matomo](https://matomo.org/) instance to generate basic website visit statistics. IP addresses are anonymized beforehand. Log data is discarded afterwards.

No data will ever be shared with any third parties.

----

## Funding

This project is funded through the [NGI Assure Fund](https://nlnet.nl/assure), a fund established by [NLnet](https://nlnet.nl) with financial support from the European Commission's [Next Generation Internet](https://ngi.eu) program. Learn more on the [NLnet project page](https://nlnet.nl/project/libresilient#ack).

<div class="partner-nav">
    <a href="https://nlnet.nl"><img src="../assets/img/nlnet-logo-banner.svg" alt="NLnet foundation logo" /></a>
    <a href="https://nlnet.nl/assure"><img src="../assets/img/ngi-assure-logo-tag.svg" alt="NGI Assure Logo" /></a>
</div>
