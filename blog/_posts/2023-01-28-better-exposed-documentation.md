---
title:  "Documentation is now more usable"
layout: post
author: rysiek
---

LibResilient's documentation used to be just a bunch of Markdown files spread across the repository, available through the [Gitlab-hosted project](https://gitlab.com/rysiekpl/libresilient/). That was fine as the first jab at making it available, but it was far from perfect: for example, it was kind of difficult to discover and browse plugin documentation.

Now, documentation (both general docs, and per-plugin documentaion) is available [directly on the website](../../docs/). Still not perfect, but considerably better nonetheless. Importantly, plugins documentation is [also gathered in a single place](../../docs/plugins/).


## What's in the docs

Documentation is divided into two parts:
 - [general documentation](../../docs/)
 - [plugins documentation](../../docs/plugins/)

This should make the information there more easily available and discoverable. There is no search (yet?), as the size of project's documentation is still small and arguably manageable with the help of a decent index pages.

### General documentation

These are documentation resources discussing LibResilient generally, or providing step-by-step guides on deployment.

This includes a high-level overview of the **[philosophy guiding the project](../../docs/PHILOSOPHY/)**, and an (still not entirely complete) **[description of its architecture](../../docs/ARCHITECTURE/)**. There is also an extensive **[Frequently Asked Questions](../../docs/FAQ/)** section, diving into things like interactions with web analytics systems and admin panels, how does LibResilient handle interactivity on a website, and a deep-dive into Service Workers as used by LibResilient.

The technical, step-by-step guides include the **[Quickstart guide](../../docs/QUICKSTART/)** and the **[example deployment](../../docs/EXAMPLE_DEPLOYMENT/)** document. There are also technical write-ups focusing on specific features of LibResilient: its ability to **[update configuration even during disruption](../../docs/UPDATING_DURING_DISRUPTION/)** and on ensuring **[security and content integrity](../../docs/CONTENT_INTEGRITY/)** — a topic particularly important when using third-party-run alternative endpoints.

<div class="home-nav">
    <a href="../../docs/">Docs</a>
</div>

### Plugins documentation

This section contains documentation of every plugin available in LibResilient's main code tree.

How extensively a plugin is documented differs between plugins. Some, like [`dnslink-fetch`](../../docs/plugins/dnslink-fetch/) offer a reasonably good write-up. Others, like [`gun-ipfs`](../../docs/plugins/gun-ipfs/). Some plugins are considered stable or late `beta`, some are broken and need to be re-written. This is clearly expressed on the plugin overview page:

<div class="home-nav">
    <a href="../../docs/plugins/">Plugins</a>
</div>

## Improving the documentation

It's important to recognize that good documentation is crucial to adoption. Quite a lot of work went into improving LibResilient's documentation situation, but by no means is it done and perfect. If you'd like to get involved in helping out with this — or with any other aspect of LibResilient — check [out the code in GitLab](https://gitlab.com/rysiekpl/libresilient/).

General documentation available on this website is built from the content in the [`/docs/` directory in the repository](https://gitlab.com/rysiekpl/libresilient/-/tree/master/docs). Plugin documentation is built from `README.md` files in [each individual plugin's directory](https://gitlab.com/rysiekpl/libresilient/-/tree/master/plugins). And if you're wondering how that's achieved, the [absolutely horrendously ugly code for that is here](https://gitlab.com/rysiekpl/resilient.is/-/blob/main/.gitlab-ci.yml). Actual LibResilient code is [much cleaner](https://gitlab.com/rysiekpl/libresilient/-/blob/master/service-worker.js), pinky promise!
