---
title:  "New DNSLink-based transport plugins"
layout: post
author: rysiek
---

After a long break (perhaps a bit too long, in fact), LibResilient is back in active development. As part of that, a two new [transport plugins](../docs/ARCHITECTURE/#transport-plugins) got implemented:

- [`dnslink-fetch`](../docs/plugins/dnslink-fetch/)
- [`dnslink-ipfs`](../docs/plugins/dnslink-ipfs/)

They fetch content using means that have been employed by LibResilient plugins before, but use [DNSLink](https://dnslink.org/) to figure out where to fetch content from.


## What is DNSLink

DNSLink is a standard for storing information on where a content related to a given domain can be found directly in DNS, using `TXT` records.

Let's say you are running a website at `https://example.org` and want to provide information on where relevant content can be found in case, for example, the main site goes down. You could put this in some place on the site itself, but this creates a chicken-and-egg problem: information on where to get content if the site is not available is only accessible as long as the site is available.

Instead, you could use DNSLink and put that information directly in DNS. To do that you would create `TXT` records for `_dnslink.example.org` label, like so:

```dns
_dnslink.example.org.  60      IN      TXT     "dnslink=/ipfs/Qm..."
_dnslink.example.org.  60      IN      TXT     "dnslink=/https/gateway.ipfs.io/ipfs/Qm..."
_dnslink.example.org.  60      IN      TXT     "dnslink=/https/example.com/"
```

Software that understands DNSLink would take this to mean that content for `example.org` is also available directly on IPFS, or via HTTPS on specific IPFS gateways, or via HTTPS on an alternative endpoint (in this case, `example.com`). As long as DNS remains available, if client software understands DNSLink and supports the relevant transport protocols, the content could be retrieved even if `https://example.com` site itself is down.

This is where the new LibResilient plugins come in.

## New plugins

With the new plugins, LibResilient turns any modern browser into client software that understands DNSLink and can retrieve content related to a website that happens to be down (as long as that particular visitor had visited that site once before and Service Worker got loaded).

The first of the two, `dnslink-fetch`, is very similar to the [`alt-fetch`](../docs/plugins/alt-fetch/) plugin: given alternative endpoints, it performs [HTTP `fetch()`](https://developer.mozilla.org/en-US/docs/Web/API/fetch) requests to them to pull relevant content. But instead of requiring the endpoints to be configured explicitly in the `config.json` configuration file (and thus be somewhat inflexible), it pulls the endpoints from DNS, in accordance to the DNSLink standard.

The second plugin, `dnslink-ipfs`, uses DNSLink to figure out which IPFS [`CID`](https://docs.ipfs.tech/concepts/content-addressing/) to use when fetching the content from IPFS. This is necessary because IPFS uses content-addressing: when content itself changes, the address will change too. When using IPFS for content retrieval, the address of the *current*, *up-to-date* version of content must first be known. DNSLink provides a good way of making that information available, and the plugin leans on that.

## Updating DNS…

…is not, however, LibResilient's responsibility. So, if you want to use these DNSLink-based plugins, you will need to separately implement some way of updating relevant `TXT` records when content gets modified or new content gets published. This can be done via your DNS hosting provider's APIs, using the [`DNS UPDATE`](https://datatracker.ietf.org/doc/html/rfc2136) query if your DNS nameservers support that, or some other means.

At least for now.

Some work has started on implementing a command-line tool that would simplify deployment of websites that use LibResilient, so perhaps one day this will be handled by the LibResilient CLI. Stay tuned!
