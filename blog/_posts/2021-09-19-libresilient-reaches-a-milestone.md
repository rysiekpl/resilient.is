---
title:  "LibResilient reaches a milestone!"
layout: post
author: rysiek
---

Took a while (summer holidays got in the way), but LibResilient finally reached a milestone: ["Improved resilience of libresilient"](https://gitlab.com/rysiekpl/libresilient/-/milestones/4). It's a reasonably big deal, for a number of reasons.

First, that means that development is actually happening again. Second, one of the goals of this milestone was to create a decent testing harness, so that any errors or breakage caused by code changes can be caught early and fixed.

Third, this is the first milestone covered by the [NLnet grant for LibResilient](https://nlnet.nl/project/libresilient/). You read that right: LibResilient development is now supported as part of the [NGI Assure](https://www.ngi.eu/ngi-projects/ngi-assure/) project. There are [six more milestones defined as part of that grant](https://gitlab.com/rysiekpl/libresilient/-/milestones/).


Biggest specific pieces of work done for this milestone focused on:

## 1. Setting up a testing system for the project, and writing the tests

 - <https://gitlab.com/rysiekpl/libresilient/-/issues/5>
 - <https://gitlab.com/rysiekpl/libresilient/-/issues/6>
 - <https://gitlab.com/rysiekpl/libresilient/-/issues/8>

Currently test coverage is [at ~53% overall](https://gitlab.com/rysiekpl/libresilient/-/jobs/1602926785#L1790). This might not sound like a lot, until one considers that for all mature plugins (`fetch`, `cache`, `any-of`, `alt-fetch`) it's at 100%, and for `service-worker.js` -- the most important and complicated piece of the project -- it's at ~95%.

This means development can go ahead with reasonable confidence, as most bugs get caught early. 

Less stable plugins (`gun-ipfs`, `ipns-ipfs`), and user-facing `libresilient.js` do not have a lot of test coverage currently, as they will require substantial rewrites in near future.

Security testing infrastructure was also set-up, which led to code quality improvements:
 - <https://gitlab.com/rysiekpl/libresilient/-/issues/13>
 - <https://gitlab.com/rysiekpl/libresilient/-/issues/12>

## 2. Re-thinking and re-writing plugin architecture.

 - <https://gitlab.com/rysiekpl/libresilient/-/issues/15>

This was a rather hairy piece of work, as it required refactoring of a lot of tightly-coupled code which was also handling core functionality of libresilient, and used to have certain old assumptions (like: *"a plugin can only be used once in the config"*) baked-in.

Having a testing harness helped a lot in making those changes without worrying if LibResilient remains functional, limiting the chance serious bugs were introduced.

This also laid down the groundwork for future work on other milestones, for example by making the config-handling code cleaner and easier to reason about.

## 3. Random fixes and improvements

Along the way some old code was re-written and random issues fixed; importantly, more error handling code was added to service-worker.js, for example:
 - <https://gitlab.com/rysiekpl/libresilient/-/blame/736675fa0673f50700a199cb3f21119c3b493f52/service-worker.js#L106>
 - <https://gitlab.com/rysiekpl/libresilient/-/blame/736675fa0673f50700a199cb3f21119c3b493f52/service-worker.js#L235>
 - <https://gitlab.com/rysiekpl/libresilient/-/blame/736675fa0673f50700a199cb3f21119c3b493f52/service-worker.js#L535>

Improved error handling means LibResilient should work better in Safari now (Safari implementing some new web APIs in the meantime also helped, of course).

## What's next?

I'm going to continue to work on LibResilient in a rather organic way -- without a very strict plan. There's plenty to be done, and basically all planned improvements are intertwined with one another. Setting up a proper website for the project, with some demos and examples, is high on the list though.
